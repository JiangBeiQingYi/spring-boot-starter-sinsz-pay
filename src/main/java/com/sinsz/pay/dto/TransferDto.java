package com.sinsz.pay.dto;

/**
 * 付款信息
 * @author chenjianbo
 */
public class TransferDto {

    /**
     * 商家自定义打款单号
     */
    private String partnerTradeNo;

    /**
     * 支付平台付款单号
     */
    private String paymentNo;

    /**
     * 付款成功时间
     */
    private String paymentTime;

    public String getPartnerTradeNo() {
        return partnerTradeNo;
    }

    public void setPartnerTradeNo(String partnerTradeNo) {
        this.partnerTradeNo = partnerTradeNo;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }
}
