package com.sinsz.pay.support;

/**
 * 全局常量
 * @author chenjianbo
 */
public interface Constant {

    /**
     * 订单支付过期间隔
     * <p>
     *     默认30分钟
     * </p>
     */
    int ORDER_EXPIRATION_INTERVAL = 30 * 60 * 1000;

    /**
     * 默认成功状态描述
     */
    String OK = "SUCCESS";

    /**
     * 失败状态描述
     */
    String FAIL = "FAIL";

}
